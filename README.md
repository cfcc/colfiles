README for ColFiles
===================

This project provides a wrapper for the UniObjects for Java library
that let's the programmer create the data files/tables as a Java class
and access the fields as class members.

Installing Maven Requirements
-----------------------------

 * tl/dr: run `repository.sh` to fix the asjava.jar dependency

A 3rd party JAR, UniObjects for Java  must be installed in the local
maven repository (`~/.m2/`)before this project can be built.

For example:

	mvn install:install-file -Dfile=lib/asjava.jar \
	-DgroupId=com.IBM.UniObjects \
	-DartifactId=uniobjects-java \
	-Dversion=10.2 \
	-Dpackaging=jar

The other libraries in `./lib` can also be installed into the maven
repository using the `repository.sh` script (required by ColleagueLdapTools).

Author
------

Jakim Friant
2016-03-21
