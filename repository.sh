#!/usr/bin/env bash
# Template:
#
# mvn install:install-file \
# -Dfile= \
# -DgroupId= \
# -DartifactId= \
# -Dversion= \
# -Dpackaging=jar

mvn install:install-file -Dfile=lib/asjava.jar \
-DgroupId=com.IBM.UniObjects \
-DartifactId=uniobjects-java \
-Dversion=10.2 \
-Dpackaging=jar

mvn install:install-file -Dfile=lib/asjava-2015.jar \
-DgroupId=com.IBM.UniObjects \
-DartifactId=uniobjects-java \
-Dversion=5.110.1 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/commons-codec-1.5.jar \
-DgroupId=commons-codec \
-DartifactId=commons-codec \
-Dversion=1.5 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/gdata-appsforyourdomain-1.0.jar \
-DgroupId=gdata-appsforyourdomain \
-DartifactId=gdata-appsforyourdomain \
-Dversion=1.0 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/gdata-appsforyourdomain-meta-1.0.jar \
-DgroupId=gdata-appsforyourdomain-meta \
-DartifactId=gdata-appsforyourdomain-meta \
-Dversion=1.0 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/gdata-base-1.0.jar \
-DgroupId=gdata-base \
-DartifactId=gdata-base \
-Dversion=1.0 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/gdata-client-1.0.jar \
-DgroupId=gdata-client \
-DartifactId=gdata-client \
-Dversion=1.0 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/gdata-client-meta-1.0.jar \
-DgroupId=gdata-client-meta \
-DartifactId= \
-Dversion= \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/gdata-client-meta-1.0.jar \
-DgroupId=gdata-client-meta \
-DartifactId=gdata-client-meta \
-Dversion=1.0 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/gdata-core-1.0.jar \
-DgroupId=gdata-core \
-DartifactId=gdata-core \
-Dversion=1.0 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/guava-11.0.2.jar \
-DgroupId=guava \
-DartifactId=guava \
-Dversion=11.0.2 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/java-getopt-1.0.13.jar \
-DgroupId=java-getopt \
-DartifactId=java-getopt \
-Dversion=1.0.13 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/jsr305.jar \
-DgroupId=jsr \
-DartifactId=jsr \
-Dversion=305 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/not-yet-commons-ssl-0.3.9.jar \
-DgroupId=not-yet-commons-ssl \
-DartifactId=not-yet-commons-ssl \
-Dversion=0.3.9 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/ostermillerutils_1_05_00_for_java_1_4.jar \
-DgroupId=ostermillerutils \
-DartifactId=ostermillerutils \
-Dversion=1.05.00 \
-Dpackaging=jar

mvn install:install-file \
-Dfile=lib/servlet.jar \
-DgroupId=javax-servlet \
-DartifactId=javax-servlet \
-Dversion=1.0 \
-Dpackaging=jar
