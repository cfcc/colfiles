/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.cfcc.colfile;

import asjava.uniclientlibs.UniStringException;
import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a generic column in a database file.  The conversion code 
 * determines what kind of data this field holds, but the actual data type as
 * far as Java is concerned is a String.
 * @author jfriant
 */
public class StringColumn extends Column {

    public StringColumn(Table t, int loc) {
        super(t, loc);
    }

    public StringColumn(Table t, int loc, String conv) {
        super(t, loc);
        conv_code = conv;
    }

    public StringColumn(Table t, int loc, String conv, String fname) {
        super(t, loc, conv, fname);
    }

    /**
     * Read and return the value held in this field.  The conversion code is
     * applied before the string is returned.
     * @return A string with the contents of the field.
     * @throws UniFileException if the read fails
     * @throws UniSessionException if the database connection fails
     */
    public String get() throws UniFileException, UniSessionException {
        String result = read();
        if (!conv_code.equals("")) {
            try {
                if (parent.current_session != null) {
                    result = parent.current_session.oconv(result, conv_code).toString();
                }
            } catch (UniStringException ex) {
                int status = parent.current_session.status();
                Logger.getLogger(Column.class.getName()).log(Level.WARNING, "status=" + status, ex);
            }
        }
        return result;
    }

    /**
     * Set the value of this field.
     * @param value A string with a value to save in this field.
     * @throws asjava.uniobjects.UniFileException if the write fails
     * @throws UniSessionException if the database connection fails
     */
    public void set(String value) throws UniFileException, UniSessionException {
        write(value);
    }
}
