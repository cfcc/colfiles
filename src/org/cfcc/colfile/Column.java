/*
 * Copyright 2007-2010 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.cfcc.colfile;

import asjava.uniclientlibs.UniString;
import asjava.uniclientlibs.UniTokens;
import asjava.uniobjects.UniFile;
import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Base class for all the Column types.
 * @author jfriant
 */
public class Column {

    /** UniData record lock code used when writing */
    public static final int EXCLUSIVE_UPDATE = 1;
    /** The number of attempts to make when trying to write to a locked record */
    public static final int MAX_ATTEMPTS = 3;
    /** The number of milliseconds to wait before checking a locked record again */
    public static final int RECORD_LOCK_WAIT = 3000;
    /** The UniData error code indicating a record was locked */
    private static final int ERR_RECORD_NOT_FOUND = 30001;
    Table parent;
    char type;
    int location;
    String conv_code;
    int length; // do we need this anyway?
    char alignment;
    String dbname = "";

    /**
     * Initialize the Column class with a parent table and the field location.
     * @param t The table (UniFile) that contains this field.
     * @param loc The location number of this field.
     */
    public Column(Table t, int loc) {
        parent = t;
        type = 'D';
        location = loc;
        conv_code = "";
        length = 10;
        alignment = 'L';
    }

    /**
     * Initialize the Column class and all the caller to set the important parameters.
     * @param t The table (UniFile) that contains this field.
     * @param loc The location number of this field.
     * @param conv The conversion code for this field (can be "").
     * @param fieldname The UniData dictionary name (used for selects)
     */
    public Column(Table t, int loc, String conv, String fieldname) {
        parent = t;
        type = 'D';
        location = loc;
        conv_code = conv;
        length = 10;
        alignment = 'L';
        dbname = fieldname;
    }

    /**
     * Read a field from the given record.  To be used from get().
     * @return A string with the value of the field.
     * @throws UniFileException if there is a record error
     * @throws UniSessionException if the database connection fails
     */
    protected String read() throws UniFileException, UniSessionException {
        UniString result = new UniString("");
//        for (int attempts = 0; attempts < MAX_ATTEMPTS; attempts++) {
//            try {
                UniFile fd = parent.open();
                result = fd.readField(parent.record_id, location);
//                Logger.getLogger(Table.class.getName()).log(Level.CONFIG, "Successfully read a value");
/*            } catch (UniFileException ex) {
                int err_code = ex.getErrorCode();
                if (err_code == asjava.uniobjects.UniObjectsTokens.UNIRPC_NO_MORE_CONNECTIONS
                        || err_code == asjava.uniobjects.UniObjectsTokens.UNIRPC_REFUSED
                        || err_code == asjava.uniobjects.UniObjectsTokens.UVE_RPC_NO_CONNECTION) {
                    Logger.getLogger(Table.class.getName()).log(Level.WARNING, "RPC Connection lost, reconnecting...", ex);
                    parent.current_session.connect();
                } else {
                    throw ex;
                }
            }*/
        //}
        return result.toString();
    }

    /**
     * Write a string value to a field in the current record.  To be used by set().
     * @param value A string with data so write to the field.
     * @throws asjava.uniobjects.UniFileException if the write operation fails.
     * @throws UniSessionException if the database connection fails
     */
    protected void write(String value) throws UniFileException, UniSessionException {
        // make multiple attempts to write to the field
        boolean can_lock = true;
        UniFile fd = parent.open();
        synchronized (fd) {
            for (int attempts = 0; attempts < MAX_ATTEMPTS; attempts++) {
                try {
                    if (can_lock) {
                        fd.lockRecord(parent.record_id, EXCLUSIVE_UPDATE);
                    }
                    fd.writeField(parent.record_id, value, location);
                    // manually end the loop
                    break;
                } catch (UniFileException ex) {
                    if (ex.getErrorCode() == ERR_RECORD_NOT_FOUND) {
                        can_lock = false;
                    } else {
                        if (ex.getErrorCode() == UniTokens.UVE_RPC_NO_CONNECTION) {
                            parent.current_session.connect();
                        } else {
                            try {
                                // Calling the UniObjects wait() function
                                this.wait(RECORD_LOCK_WAIT);
                            } catch (InterruptedException ex2) {
                                String msg = "ResetPassword: Interrupted while waiting for record lock on " + this.parent.filename + " (" + this.dbname + ") for " + parent.record_id + ", attempt " + (attempts + 1);
                                Logger.getLogger(Table.class.getName()).log(Level.WARNING, msg, ex2);
                            }
                        }
                    }
                } finally {
                    fd.unlockRecord();
                }
            }
        }
    }

    public void setDatabaseName(String field_name) {
        dbname = field_name;
    }

    public String getDatabaseName() {
        String result = "";


        if (dbname.equals("")) {
            String my_name = Column.class.getName();
            result = my_name.toUpperCase();
            // generate the name based on the class name
        } else {
            result = dbname;
        }
        return result;
    }

    public String getConvCode() {
		return conv_code;
	}

	public void setConvCode(String conv_code) {
		this.conv_code = conv_code;
	}

	protected String asQuery(String comparison, String value) {
        String verb = "WITH";
        String field_name = getDatabaseName();
        String query = verb + " " + field_name + " " + comparison + " '" + value + "'";
        return query;
    }

    public String eq(String qvalue) {
        return asQuery("EQ", qvalue);
    }

    public String eq(int qvalue) {
        return asQuery("EQ", String.valueOf(qvalue));
    }
}
