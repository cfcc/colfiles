/*
 * Copyright 2007, 2008, 2009, 2010 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.cfcc.colfile;

/**
 * Raised when there is an error accessing a field through a foreign key.
 * @author jfriant
 */
public class TransColumnException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7344044670807089520L;

	public TransColumnException(String s, Throwable orig) {
        super(s, orig);
    }
}