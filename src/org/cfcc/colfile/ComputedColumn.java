package org.cfcc.colfile;

import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;
import asjava.uniobjects.UniFile;
import asjava.uniclientlibs.UniString;


public class ComputedColumn extends Column {
    public ComputedColumn(Table t, String field_name) {
        super(t, 0, "", field_name); }

    public String get() throws UniFileException, UniSessionException {
        UniFile fd = parent.open();
        UniString result = fd.iType(parent.record_id, dbname);

        return result.toString();
    }
    public String set() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
