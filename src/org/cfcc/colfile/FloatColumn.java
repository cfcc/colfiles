/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.cfcc.colfile;

import asjava.uniclientlibs.UniStringException;
import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a numeric column with floating point data in a database file. The
 * conversion code defaults to "MD2" but can be changed in the constructor.
 * 
 * IMPORTANT: Java's Float.valueOf does not accept commas as part of the number,
 * so do not use "MD2," as the conversion code, for example.
 * 
 * @author jfriant
 */
public class FloatColumn extends Column {

	public FloatColumn(Table t, int loc) {
		super(t, loc);
		conv_code = "MD2";
	}

	public FloatColumn(Table t, int loc, String conv) {
		super(t, loc);
		conv_code = conv;
	}

	public FloatColumn(Table t, int loc, String conv, String fname) {
		super(t, loc, conv, fname);
	}

	/**
	 * Read and return the value held in this field. The conversion code is
	 * applied before the value is returned.
	 * 
	 * @return An integer with the value of the field.
	 * @throws UniFileException if there is an error reading the record
	 * @throws UniSessionException if the database connection fails
	 */
	public float get() throws UniFileException, UniSessionException {
		float result = 0.0f;
		String contents = read();
		try {
			if (parent.current_session != null) {
				result = Float.valueOf(parent.current_session.oconv(contents, conv_code).toString());
			}
		} catch (UniStringException ex) {
			int status = parent.current_session.status();
			Logger.getLogger(Column.class.getName()).log(Level.WARNING, "status=" + status, ex);
		} catch (java.lang.NumberFormatException ex) {
			Logger.getLogger(Column.class.getName()).log(Level.WARNING, "invalid float format: " + contents, ex);
		}
		return result;
	}

	/**
	 * Set the value of this field.
	 * 
	 * @param value
	 *            An integer with a value to save in this field.
	 * @throws asjava.uniobjects.UniFileException
	 *             If the write fails.
	 * @throws UniSessionException
	 *             If the database connection fails
	 */
	public void set(int value) throws UniFileException, UniSessionException {
		Integer val = new Integer(value);
		write(val.toString());
	}
}
