/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.cfcc.colfile;

import asjava.uniclientlibs.UniString;
import asjava.uniobjects.UniFile;
import asjava.uniobjects.UniFileException;

/**
 *
 * @author jfriant
 */
public class TransColumn extends Column {

    private String filename;
    private int key_location;
    private boolean strict = false;

    public TransColumn(Table t, String filename, int location) {
        super(t, location);
        this.filename = filename;
        this.key_location = 0;
    }

    public TransColumn(Table t, int key_loc, String fn, int loc) {
        super(t, loc);
        filename = fn;
        key_location = key_loc;
    }

    /**
     * Create the class with all the options set in the function call.
     * @param t is the table class (i.e. "this")
     * @param key_loc is the field number with the key value 
     * @param fn is the filename of the "remote" file
     * @param loc is the field number in the remote file
     * @param strict is a flag that when set to true with raise an exception if the value is not fetched correctly
     */
    public TransColumn(Table t, int key_loc, String fn, int loc, boolean strict) {
        super(t, loc);
        filename = fn;
        key_location = key_loc;
        this.strict = strict;
    }

    public String get() throws TransColumnException {
        UniString result = new UniString("");
        UniString key_value = new UniString("");
        //System.err.println("TransColumn.get(): record_id=" + parent.record_id + ", key_location=" + key_location);
        try {
            try {
            	UniFile fd = parent.open();
                key_value = fd.readField(parent.record_id, key_location);
            } catch (UniFileException ex) {
            	if (strict) {
            		String error_location = "Error reading key value: " + parent.filename + "[" + parent.record_id + "]";
            		throw new TransColumnException(error_location, ex);
            	}
            }
            //System.err.println("TransColumn.get(): filename=" + filename + ", key_value=" + key_value + ", location=" + location);
            UniFile fd = new UniFile(parent.current_session, filename, 0);
            result = fd.readField(key_value, location);
        } catch (UniFileException ex) {
        	if (strict) {
        		String error_location = "Error reading data: " + filename + "[" + key_value.toString() + "]";
        		throw new TransColumnException(error_location, ex);
        	}
        }
        return result.toString();
    }

    public String set() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
