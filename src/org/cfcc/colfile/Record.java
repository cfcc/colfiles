/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.cfcc.colfile;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: This class is not ready for use yet.  
 * @author jfriant
 */
public class Record {
    String record_id;
    Map<?, ?> fields;
    
    public Record() {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    public Record(Table parent, String rec_id) {
        record_id = rec_id;
        fields = Collections.synchronizedMap(new HashMap<Object, Object>());
        //fields.putAll(parent.fields);
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    public String get(String field_id) {
        // StringBuilder is not thread-safe, so I use StringBuffer here.
//        StringBuffer result = new StringBuffer("");
//        if (fields.containsKey(field_id)) {
            // do something with fields[field_id]
//        }
        throw new UnsupportedOperationException("Not yet implemented");
//        return result.toString();
    }
}
