/**
 * Copyright 2007, 2008, 2009, 2010 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.cfcc.colfile;

import asjava.uniclientlibs.UniString;
import asjava.uniobjects.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a Colleague file as a Java object, with methods and accessors
 * representing the fields in a unidata file.
 * @author jfriant
 */
public class Table {

    UniSession current_session = null;
    int is_dict = 0;
    UniFile fd = null;
    String record_id = "";
    String filename = null;

	public Table(UniSession s, String fn) {
        current_session = s;
        filename = fn;
    }

    public String getId() {
        return record_id;
    }

    public void setId(String rec_id) {
        record_id = rec_id;
    }

    public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

    public Record get(String rec_id) {
        /* For now I'm am using the fields to get values for a record.  The 
         * fields have a data member that references the parent table.
         */
        //return new Record(this, rec_id);
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public ArrayList<String> select() {
        return do_select("*", 1);
    }

    public ArrayList<String> select(String query) {
        return do_select(query, 1);
    }

    public ArrayList<String> select(String query, int select_list_num) {
        return do_select(query, select_list_num);
    }

    private ArrayList<String> do_select(String query, int select_list_num) {
        ArrayList<String> record_ids = new ArrayList<String>();
        StringBuffer q = new StringBuffer("SELECT ");
        try {
            UniSelectList u_sel = current_session.selectList(select_list_num);
            if (query.equals("*")) {
                fd = this.open();
                u_sel.select(fd);
            } else {
                q.append(filename).append(" ").append(query).append(" TO ").append(select_list_num);
                UniCommand cmd = null;
                cmd = current_session.command();
                cmd.setCommand(q.toString());
                cmd.exec();
                int result = cmd.getSystemReturnCode();
                if (result <= 0) {
                    Logger.getLogger(Table.class.getName()).log(Level.WARNING, "No records returned from select statement: {0})", cmd.getCommand());
                    Logger.getLogger(Table.class.getName()).log(Level.INFO, "Unidata response: {0}", cmd.response());
                }
            }
            // do a readnext to retrieve the select list of record IDs
            while (!u_sel.isLastRecordRead()) {
                try {
                    UniString rec_id = u_sel.next();
                    if (rec_id != null && !rec_id.toString().equals("")) {
                        record_ids.add(rec_id.toString());
                    }
                } catch (UniSelectListException ex) {
                    Logger.getLogger(Table.class.getName()).log(Level.SEVERE, "Error with readnext", ex);
                }
            }
        } catch (UniSessionException ex) {
            Logger.getLogger(Table.class.getName()).log(Level.SEVERE, "command or READNEXT error", ex);
        } catch (UniFileException ex) {
            Logger.getLogger(Table.class.getName()).log(Level.SEVERE, "file select error", ex);
        } catch (UniSelectListException ex) {
            Logger.getLogger(Table.class.getName()).log(Level.SEVERE, "file select error", ex);
        } catch (UniCommandException ex) {
            Logger.getLogger(Table.class.getName()).log(Level.SEVERE, "error in select statement: " + q, ex);
        }
        return record_ids;
    }

    public UniFile open() throws UniFileException {
        if (fd == null) {
            fd = new UniFile(current_session, filename, is_dict);
        }
        if (!fd.isOpen()) {
            fd.open();
        }
        return fd;
    }

    public void close() {
        if (fd != null) {
            try {
                fd.close();
            } catch (UniFileException ex) {
                Logger.getLogger(Table.class.getName()).log(Level.CONFIG, null, ex);
            }
        }
    }

    public void unlockRecord() throws UniFileException {
        fd.unlockRecord(record_id);
    }
}
