/*
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.cfcc.colfile;

import asjava.uniclientlibs.UniStringException;
import asjava.uniclientlibs.UniStringTokenizer;
import asjava.uniclientlibs.UniTokens;
import asjava.uniobjects.UniFileException;
import asjava.uniobjects.UniSessionException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a multivalued field in a database file.
 * @author jfriant
 */
public class ListColumn extends Column {

    public ListColumn(Table t, int loc) {
        super(t, loc);
    }

    public ListColumn(Table t, int loc, String conv, String fname) {
        super(t, loc, conv, fname);
    }

    public ListColumn(Table t, int loc, String conv) {
		super(t, loc);
		conv_code = conv;
	}

	public List<String> get() throws UniFileException, UniSessionException {
        String result = read();
        List<String> results = Collections.synchronizedList(new ArrayList<String>());
        synchronized (results) {
            UniStringTokenizer tok = new UniStringTokenizer(result, UniTokens.VM_CHAR);
            while (tok.hasMoreTokens()) {
                String value = tok.nextToken();
                if (!conv_code.equals("")) {
                    try {
                        value = parent.current_session.oconv(value, conv_code).toString();
                    } catch (UniStringException ex) {
                        Logger.getLogger(Column.class.getName()).log(Level.WARNING, null, ex);
                    }
                }
                boolean add = results.add(value);
                if (add == false) {
                    break;
                }
            }
        }
        return results;
    }

    public void set(List<?> values) throws UniFileException, UniSessionException {
        // StringBuilder is not thread-safe, so I use StringBuffer here.
        StringBuffer dyn_arr = new StringBuffer("");
        boolean first = true;
        ListIterator<?> itr = values.listIterator();
        while (itr.hasNext()) {
            dyn_arr.append((String) itr.next());
            if (first) {
                first = false;
            } else {
                dyn_arr.append(UniTokens.AT_VM);
            }
        }
        write(dyn_arr.toString());
    }
}
